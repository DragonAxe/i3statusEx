# README #

A small Scala program to replace the battery percentage with a battery bar. i.e. [   ####|60%]

### How do I get set up? ###

* In the source code, change the i3status execution command to point to your i3status.conf file.
* Start sbt in the project's root directory.
* Run the sbt action: compile
* In your i3 config, set the i3bar's status_command property to something like scala -cp ~/dir/to/project I3StatusEx
* Reload i3 with Mod+Shift+r

### Who do I talk to? ###

* Repo owner or admin